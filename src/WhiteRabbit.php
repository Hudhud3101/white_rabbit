<?php

class WhiteRabbit
{
    protected $letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ä', 'æ', 'é', 'ü'];
    
    public function findMedianLetterInFile($filePath)
    {
        $content = $this->parseFile($filePath);
        $occurrences = 0;
        
        return array(
            "letter" => $this->findMedianLetter($this->parseFile($filePath),$occurrences),
            "count" => $occurrences
        );
    }
    
    /**
     * Parse the input file for letters.
     * @param string $filePath
     *
     * @return array List of letters
     */
    private function parseFile ($filePath)
    {
        $fh = $this->openFile($filePath, 'r');
        $content = '';
        
        while($line = $this->getLineFromFile($fh)) {
            $content .= preg_replace('/[^\a-zäæéü]/', '', $line);
        }
        
        $this->closeFile($fh);
        
        return $content;
    }
    
    
    
    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        $counts = $this->countSubstringsInString($parsedFile, $this->letters);
        $zero = [0];
        $counts = array_diff($counts, $zero);
        
        asort($counts, SORT_NUMERIC);
        
        $keys = array_keys($counts);
        $keyIndex = floor((count($keys) - 1) / 2);
        $key = $keys[$keyIndex];
        
        $occurrences = $counts[$key];
        return $key;
    }
    
    protected function countSubstringsInString(string $string, array $substrings): array
    {
        $counts = [];
        foreach ($substrings as $_substring) {
            $counts[$_substring] = mb_substr_count(strtoupper($string), strtoupper($_substring));
        }
        
        return $counts;
    }
    
    
    protected function openFile($path, $mode)
    {
        $fh = fopen($path, $mode);
        
        return $fh;
    }
    
    protected function getLineFromFile($handle)
    {
        return fgets($handle);
    }
    
    protected function closeFile($handle)
    {
        return fclose($handle);
    }
    
}